var searchData=
[
  ['actbdim_0',['ActBDim',['../class_min_quad__di__unipi__it_1_1_min_quad.html#aceef4cf75228f7e08be50b1d2c063186',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['actcnum_1',['ActCNum',['../class_min_quad__di__unipi__it_1_1_min_quad.html#aa28a23b6c915fcf93bb7773e0383e60f',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['alfa_2',['Alfa',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a4fcb6e67d00d784b1680bbef5d50d86c',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['algorithm_3',['algorithm',['../class_n_d_o__di__unipi__it_1_1_o_s_i_m_p_solver.html#a6e70abc910d9f3bfaefd624583dc1ed8',1,'NDO_di_unipi_it::OSIMPSolver']]],
  ['alpha_4',['alpha',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#abba4c55441c1944f3b0696d367152e04',1,'NDO_di_unipi_it::SubGrad']]],
  ['aset_5',['Aset',['../class_n_d_o__di__unipi__it_1_1_o_s_i_m_p_solver.html#a2ec7925d07dc33813a074e8e322dd25f',1,'NDO_di_unipi_it::OSIMPSolver']]],
  ['asetdim_6',['Asetdim',['../class_n_d_o__di__unipi__it_1_1_o_s_i_m_p_solver.html#ac4c7ad7ade73639341783d7cdab47343',1,'NDO_di_unipi_it::OSIMPSolver']]]
];
