var searchData=
[
  ['pariter_0',['ParIter',['../class_n_d_o__di__unipi__it_1_1_n_d_o_solver.html#ac9716eb40fc916cad7d66d31cb22be80',1,'NDO_di_unipi_it::NDOSolver']]],
  ['polyak_1',['Polyak',['../class_n_d_o__di__unipi__it_1_1_polyak.html',1,'Polyak'],['../class_n_d_o__di__unipi__it_1_1_polyak.html#ad01bc87df38fb9df8908700fffef418c',1,'NDO_di_unipi_it::Polyak::Polyak()']]],
  ['polyak_2eh_2',['Polyak.h',['../_polyak_8h.html',1,'']]],
  ['primaldual_3',['PrimalDual',['../class_n_d_o__di__unipi__it_1_1_primal_dual.html',1,'PrimalDual'],['../class_n_d_o__di__unipi__it_1_1_primal_dual.html#aea8b2bb271a107761441b7529b31853e',1,'NDO_di_unipi_it::PrimalDual::PrimalDual()']]],
  ['primaldual_2eh_4',['PrimalDual.h',['../_primal_dual_8h.html',1,'']]],
  ['prvsti_5',['PrvsTi',['../class_min_quad__di__unipi__it_1_1_min_quad.html#ab737d1a31f3ab7dd0e3256a6c9918788',1,'MinQuad_di_unipi_it::MinQuad']]]
];
