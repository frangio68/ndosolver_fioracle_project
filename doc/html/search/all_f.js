var searchData=
[
  ['qppenaltymp_0',['QPPenaltyMP',['../class_n_d_o__di__unipi__it_1_1_q_p_penalty_m_p.html',1,'NDO_di_unipi_it']]],
  ['qppnltmp_2eh_1',['QPPnltMP.h',['../_q_p_pnlt_m_p_8h.html',1,'']]],
  ['qpstatus_2',['QPStatus',['../class_min_quad__di__unipi__it_1_1_min_quad.html#af4cc87b4aeb51784f7c9961c50e9c50d',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['quad_3',['Quad',['../class_min_quad__di__unipi__it_1_1_min_quad.html#ae735054192b74f36bbc75e84fc014ee3',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['qumat_4',['QuMat',['../group___o_p_t_t_y_p_e_s___s_u_b_g_t.html#ga30ed0a18e32c466c12fdaa82e54f3b86',1,'OPTtypes_di_unipi_it']]],
  ['qunum_5',['QuNum',['../group___o_p_t_t_y_p_e_s___s_u_b_g_t.html#ga7ac4fd0feaf2c45ed93ab7967bd05f26',1,'OPTtypes_di_unipi_it']]],
  ['qurow_6',['QuRow',['../group___o_p_t_t_y_p_e_s___s_u_b_g_t.html#gadc20a9830e750755becc3164952c4c8d',1,'OPTtypes_di_unipi_it']]]
];
