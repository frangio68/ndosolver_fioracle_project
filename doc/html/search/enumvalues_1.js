var searchData=
[
  ['kfatal_0',['kFatal',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7aa1e6c11c0353dcf8096dcfa378deb0c6',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['kincreasingstep_1',['kIncreasingStep',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7a0796554fc724f3acd76bd5791059b11f',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['kinvalidv_2',['kInvalidV',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7a17d7b6c96d7c3613907e19973f665fc1',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['kloop_3',['kLoop',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7a33d1ecef71514a9ce7059403289f31a8',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['knegativedelta_4',['kNegativeDelta',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7a39a3d1060530aab80301666e9717f5f2',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['knegvarinbase_5',['kNegVarInBase',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7ae28992a96e277478509c3304a41f1f17',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['kok_6',['kOK',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7a42e6b26684acf67af002df88a9cf997c',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['kqpprimunbndd_7',['kQPPrimUnbndd',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7a5db5f91b4caa9e91692da12042854caa',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['kstptime_8',['kStpTime',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a858b2700cb35d34520f08f8defe8c8f7aa11511fd69416861a69d9b17eda309c2',1,'MinQuad_di_unipi_it::MinQuad']]]
];
