var searchData=
[
  ['deflection_0',['deflection',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#aebf3d85908a2f88c645724123e8cb82c',1,'NDO_di_unipi_it::SubGrad']]],
  ['deflection_1',['Deflection',['../class_n_d_o__di__unipi__it_1_1_deflection.html#a627f9dee0898f5eaa87a05f2844d6014',1,'NDO_di_unipi_it::Deflection::Deflection()'],['../class_n_d_o__di__unipi__it_1_1_deflection.html',1,'Deflection']]],
  ['deflection_2eh_2',['Deflection.h',['../_deflection_8h.html',1,'']]],
  ['deleted_3',['Deleted',['../class_n_d_o__di__unipi__it_1_1_fi_oracle.html#aa80f5c831464c830e75d9a86c18eb1e3',1,'NDO_di_unipi_it::FiOracle']]],
  ['delta_4',['Delta',['../class_n_d_o__di__unipi__it_1_1_deflection.html#aad3f899ea842432b62d606463a2425b5',1,'NDO_di_unipi_it::Deflection::Delta()'],['../class_n_d_o__di__unipi__it_1_1_volume.html#aae80bd475408be2c3c46b54487fa7af6',1,'NDO_di_unipi_it::Volume::Delta()']]],
  ['description_20of_20_22easy_22_20components_20of_20fi_28_29_5',['Description of &quot;easy&quot; components of Fi()',['../group___easy_comp.html',1,'']]],
  ['dfltdsfinpt_6',['DfltdSfInpt',['../group___o_p_t_utils___f_u_n_c_t_i_o_n_s.html#ga797f93c9ea2c57c5f7f7e528109886b3',1,'OPTtypes_di_unipi_it']]],
  ['dgk_7',['dGk',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#affcc51ad17362aa7a7bedd34dc33eb6e',1,'NDO_di_unipi_it::SubGrad']]],
  ['dict_5fitem_8',['dict_item',['../class_n_d_o__di__unipi__it_1_1_o_s_i_m_p_solver.html#a5d08319d483b4827dca99b26f386afc8',1,'NDO_di_unipi_it::OSIMPSolver']]],
  ['dict_5fslack_9',['dict_slack',['../class_n_d_o__di__unipi__it_1_1_o_s_i_m_p_solver.html#a0cf62ee37819046cef6bbc284f11b4f3',1,'NDO_di_unipi_it::OSIMPSolver']]],
  ['dict_5fstab_10',['dict_stab',['../class_n_d_o__di__unipi__it_1_1_o_s_i_m_p_solver.html#a8c884e53daeeb9b7aafe83a60a0cafa9',1,'NDO_di_unipi_it::OSIMPSolver']]],
  ['dir_11',['dir',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#a69d80e390f5c7d2bdce2b6a1161727d2',1,'NDO_di_unipi_it::SubGrad']]],
  ['dirpos_12',['DirPos',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#a30290cf64fe12c1d416a28ac492ae4bf',1,'NDO_di_unipi_it::SubGrad']]],
  ['dm1gk_13',['dM1Gk',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#a39757987fa50984a045584405f345fef',1,'NDO_di_unipi_it::SubGrad']]],
  ['dm1gkdone_14',['dM1GkDone',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#a566285d3c7a54180fe19b96ef5e48741',1,'NDO_di_unipi_it::SubGrad']]],
  ['doss_15',['DoSS',['../class_n_d_o__di__unipi__it_1_1_sub_grad.html#a09a51cf4a1bb9a39e8ac6927d013ae2d',1,'NDO_di_unipi_it::SubGrad::DoSS'],['../class_n_d_o__di__unipi__it_1_1_deflection.html#ad55f518a4f14a5a33c1aec6a69c33925',1,'NDO_di_unipi_it::Deflection::DoSS()'],['../class_n_d_o__di__unipi__it_1_1_volume.html#a3010295321bfcdbe0f32817808167b91',1,'NDO_di_unipi_it::Volume::DoSS()']]],
  ['dperg_16',['DPerG',['../class_min_quad__di__unipi__it_1_1_b_min_quad.html#a65442ec74ba34a7d3d1dc88fbae8f955',1,'MinQuad_di_unipi_it::BMinQuad']]]
];
