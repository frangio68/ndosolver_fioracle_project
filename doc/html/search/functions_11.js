var searchData=
[
  ['ubvar_0',['UBVar',['../class_min_quad__di__unipi__it_1_1_b_min_quad.html#a07d975c35af0b9dcc98d28aca8e8d3f2',1,'MinQuad_di_unipi_it::BMinQuad']]],
  ['updateb_1',['UpdateB',['../class_min_quad__di__unipi__it_1_1_min_quad.html#a205516799a1ffe79c5425a5c49768f73',1,'MinQuad_di_unipi_it::MinQuad']]],
  ['updatetargetlevel_2',['UpdateTargetLevel',['../class_n_d_o__di__unipi__it_1_1_stepsize.html#aa423e63f3ebff990a00f8b6c17e2ea9a',1,'NDO_di_unipi_it::Stepsize::UpdateTargetLevel()'],['../class_n_d_o__di__unipi__it_1_1_color_t_v.html#ab5558904f759fbdbc796d269f5177852',1,'NDO_di_unipi_it::ColorTV::UpdateTargetLevel()'],['../class_n_d_o__di__unipi__it_1_1_fumero_t_v.html#ab5558904f759fbdbc796d269f5177852',1,'NDO_di_unipi_it::FumeroTV::UpdateTargetLevel()'],['../class_n_d_o__di__unipi__it_1_1_polyak.html#ab5558904f759fbdbc796d269f5177852',1,'NDO_di_unipi_it::Polyak::UpdateTargetLevel()']]],
  ['upperbounds_3',['UpperBounds',['../class_min_quad__di__unipi__it_1_1_b_min_quad.html#a8bbacc20e942404563a5c1ff41a6f91e',1,'MinQuad_di_unipi_it::BMinQuad']]]
];
