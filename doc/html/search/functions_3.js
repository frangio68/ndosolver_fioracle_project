var searchData=
[
  ['deflection_0',['Deflection',['../class_n_d_o__di__unipi__it_1_1_deflection.html#a627f9dee0898f5eaa87a05f2844d6014',1,'NDO_di_unipi_it::Deflection']]],
  ['deleted_1',['Deleted',['../class_n_d_o__di__unipi__it_1_1_fi_oracle.html#aa80f5c831464c830e75d9a86c18eb1e3',1,'NDO_di_unipi_it::FiOracle']]],
  ['delta_2',['Delta',['../class_n_d_o__di__unipi__it_1_1_deflection.html#aad3f899ea842432b62d606463a2425b5',1,'NDO_di_unipi_it::Deflection::Delta()'],['../class_n_d_o__di__unipi__it_1_1_volume.html#aae80bd475408be2c3c46b54487fa7af6',1,'NDO_di_unipi_it::Volume::Delta()']]],
  ['dfltdsfinpt_3',['DfltdSfInpt',['../group___o_p_t_utils___f_u_n_c_t_i_o_n_s.html#ga797f93c9ea2c57c5f7f7e528109886b3',1,'OPTtypes_di_unipi_it']]],
  ['doss_4',['DoSS',['../class_n_d_o__di__unipi__it_1_1_deflection.html#ad55f518a4f14a5a33c1aec6a69c33925',1,'NDO_di_unipi_it::Deflection::DoSS()'],['../class_n_d_o__di__unipi__it_1_1_volume.html#a3010295321bfcdbe0f32817808167b91',1,'NDO_di_unipi_it::Volume::DoSS()']]],
  ['dperg_5',['DPerG',['../class_min_quad__di__unipi__it_1_1_b_min_quad.html#a65442ec74ba34a7d3d1dc88fbae8f955',1,'MinQuad_di_unipi_it::BMinQuad']]]
];
