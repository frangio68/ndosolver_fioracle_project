##############################################################################
################################ makefile ####################################
##############################################################################
#                                                                            #
#   makefile of test (for TestFi)                                            #
#                                                                            #
#                              Antonio Frangioni                             #
#                         Dipartimento di Informatica                        #
#                             Universita' di Pisa                            #
#                                                                            #
##############################################################################

# module name
NAME = TF_test

# basic directory
DIR = .

# common flags
COMMON_SW = -std=c++14

# debug switches
SW_DEBUG = -g -ferror-limit=1 $(COMMON_SW)
# production switches
SW_RELEASE = -O3 -DNDEBUG $(COMMON_SW)

# compiler
CC = clang++

# default target- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

default: release

# debug target- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

debug: SW = $(SW_DEBUG)
debug: $(NAME)

# release target- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

release: SW = $(SW_RELEASE)
release: $(NAME)

# clean target- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

clean::
	rm -f $(DIR)/*.o $(DIR)/*~ $(NAME)

# define & include the necessary modules- - - - - - - - - - - - - - - - - - -
# if a module is not used in the current configuration, just comment out the
# corresponding include line
# each module outputs some macros to be used here:
# *OBJ is the final object(s) / library
# *LIB is the external libraries + -L< libdirs >
# *H   is the list of all include files
# *INC is the -I< include directories >

# libNDO
libNDOSDR = ../..
include $(libNDOSDR)/lib/makefile-inc

TstFiSDR = ..
include $(TstFiSDR)/makefile

# main module (linking phase) - - - - - - - - - - - - - - - - - - - - - - - -

# object files
MOBJ =  $(libNDOOBJ) $(TstFiOBJ)

# libraries
MLIB =  $(libNDOLIB) $(TstFiLIB)

$(NAME): $(MOBJ) $(DIR)/test.o
	$(CC) -o $(NAME) $(DIR)/test.o $(MOBJ) $(MLIB) $(SW)

# dependencies: every .o from its .cpp + every recursively included .h- - - -

# include dirs
MINC =  $(libNDOINC) $(TstFiINC)

# includes
MH =    $(libNDOH) $(TstFiH)

$(DIR)/test.o: $(DIR)/test.cpp $(MH)
	$(CC) -c $*.cpp -o $@ $(MINC) $(SW)

# distclean target- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

distclean: clean

# phony targets - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

.PHONY: debug release clean distclean

############################ End of makefile #################################
